package gr.mycompany.demo.Controller;

import gr.mycompany.demo.Model.Employee;
import gr.mycompany.demo.Service.EmployeeService;
import gr.mycompany.demo.util.CustomErrorType;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j

public class RestApiController {
    @Autowired
    EmployeeService employeeService;

    /**
     * This is for testing
     * @return a string for test http response
     */
    @RequestMapping(value = "/testme", method =RequestMethod.GET)

    public ResponseEntity<String> testme(){
        log.debug("Entering()", "testme");
        String myreturn= "Hello";
        log.debug("Leaving()","testme");
        return new ResponseEntity<String>(myreturn, HttpStatus.OK);
    }

    /**
     * This is for testing
     * @return a list for http response
     */
    @RequestMapping(value = "/employee",method=RequestMethod.GET)
    public ResponseEntity<List<Employee>> listAllUsers() {
        List<Employee>employees = employeeService.findAllEmployees();
        if(employees.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Employee>>(employees,HttpStatus.OK);
    }

    /**
     * This is for testing
     * @param id
     * @return an employee with the specific id
     */
    @RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> findEmployee(@PathVariable("id") int id) {
        log.info("Find Employee with id {}", id);

        Employee employees = (Employee) employeeService.findById(id);
        if (employees == null) {
            log.error("Employee with id {} not found.", id);
            return new ResponseEntity(
                    new CustomErrorType("Employee with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        //EmployeeService.findEmployeeById(id);
        return new ResponseEntity<Employee>(employees, HttpStatus.OK);
    }

    /**
     * This is for testing
     * @param id
     * @return
     */
    @RequestMapping(value = "/employee/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteEmployee(@PathVariable("id") int id) {
        log.info("Fetching & Deleting Employee with id {}", id);

        Employee employee = employeeService.findById(id);
        if (employee == null) {
            log.error("Unable to delete. Employee with id {} not found.", id);
            return new ResponseEntity(
                    new CustomErrorType("Unable to delete. Employee with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        employeeService.deleteEmployeeById(id);
        return new ResponseEntity<Employee>(HttpStatus.OK);
    }
}
