package gr.mycompany.demo.Service;

import gr.mycompany.demo.Model.Employee;
import java.util.List;

public interface EmployeeService {

    List<Employee> findAllEmployees();
     Employee findById(int id);
     void deleteEmployeeById(int id);
    }
