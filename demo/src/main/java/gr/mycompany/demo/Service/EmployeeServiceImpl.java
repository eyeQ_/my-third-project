package gr.mycompany.demo.Service;

import gr.mycompany.demo.Model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


@Service("employeeService")
@Slf4j

public class EmployeeServiceImpl implements EmployeeService {
    private static final AtomicLong counter = new AtomicLong();
    private static List<Employee> employees;

    static {
        employees = populateDummyUsers();
    }

    /**
     * This method finds all employees
     * @return a list of all employees
     */
    @Override
    public List<Employee> findAllEmployees() {
        return employees;
    }

    /**
     *
     * @param id
     * @return an employee with spesific id
     */
    public Employee findById(int id) {
        return employees.get(id);
    }

    /**
     * takes as input
     * @param id and deletes an employee with that id
     */
    public void deleteEmployeeById(int id) {
          employees.remove(id);
    }

    /**
     * add employees for testing
     * @return a list with employees
     */
    private static List<Employee> populateDummyUsers() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(1, "Alex"));
        employees.add(new Employee(2, "Tom"));
        employees.add(new Employee(3, "Jack"));
        employees.add(new Employee(4, "Natali"));
        return employees;
    }
}
