package gr.mycompany.demo.util;

import gr.mycompany.demo.Model.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Getter
@AllArgsConstructor

public class CustomErrorType extends Exception {
    private String errorMessage;
}

